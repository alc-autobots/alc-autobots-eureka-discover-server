FROM openjdk:8
ARG JAR_FILE
ADD ${JAR_FILE} eureka_discovery.jar
VOLUME /tmp
EXPOSE 8761
ENTRYPOINT ["java", "-jar", "/eureka_discovery.jar"]

